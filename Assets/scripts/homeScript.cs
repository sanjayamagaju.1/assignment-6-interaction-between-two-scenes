using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class homeScript : MonoBehaviour
{
    public TMP_InputField inputPlayerName;
    public TextMeshProUGUI carOption;
    public static string name;
    public static string carName;

    public void getPlayerName(){
        name = inputPlayerName.text;
    }

    public void getCarImage(){
        carName = carOption.text;
        // Debug.Log(carName);
    }
}
