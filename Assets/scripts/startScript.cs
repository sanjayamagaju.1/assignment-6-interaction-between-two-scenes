using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class startScript : MonoBehaviour
{
    public TextMeshProUGUI nameOfPlayer;
    public static string nameOfCar;
    public GameObject redCar, yellowCar, orangeCar, policeCar;

    public void Start() {
        nameOfPlayer.text = "Hello," + " " + homeScript.name;

        nameOfCar = homeScript.carName;
        Debug.Log(nameOfCar);

        if(nameOfCar == "Red car")
        {
            redCar.SetActive(true);
            yellowCar.SetActive(false);
            orangeCar.SetActive(false);
            policeCar.SetActive(false);
        }
        else if (nameOfCar == "Yellow car")
        {
            redCar.SetActive(false);
            yellowCar.SetActive(true);
            orangeCar.SetActive(false);
            policeCar.SetActive(false);
        }
        else if (nameOfCar == "Orange car")
        {
            redCar.SetActive(false);
            yellowCar.SetActive(false);
            orangeCar.SetActive(true);
            policeCar.SetActive(false);
        }
        else{
            redCar.SetActive(false);
            yellowCar.SetActive(false);
            orangeCar.SetActive(false);
            policeCar.SetActive(true);
        }
    }
    
}
